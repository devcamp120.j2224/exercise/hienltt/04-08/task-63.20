package com.devcamp.j62crudfrontend.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.Optional;

import com.devcamp.j62crudfrontend.model.CVoucher;
import com.devcamp.j62crudfrontend.repository.IVoucherRepository;

import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Service;

@Service
public class CVoucherService {
    @Autowired
	IVoucherRepository pVoucherRepository;
    
    public ArrayList<CVoucher> getVoucherList() {
        ArrayList<CVoucher> listCVoucher = new ArrayList<>();
        pVoucherRepository.findAll().forEach(listCVoucher::add);
        return listCVoucher;
    }

    public CVoucher updateCVoucherById(long id, CVoucher pVouchers){
        Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
		if (voucherData.isPresent()) {
			CVoucher voucher= voucherData.get();
			voucher.setMaVoucher(pVouchers.getMaVoucher());
			voucher.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());
			voucher.setGhiChu(pVouchers.getGhiChu());
			voucher.setNgayCapNhat(new Date());
            return voucher;
        }
        return null;
    }
}
